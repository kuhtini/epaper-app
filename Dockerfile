FROM openjdk:19-jdk-alpine
COPY target/epaper-app-0.0.1-SNAPSHOT.jar /usr/local/epaper/epaper-app-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/usr/local/epaper/epaper-app-0.0.1-SNAPSHOT.jar"]
