package com.paper.epaper.pages.controller;


import com.paper.epaper.pages.entity.PagesRequestInfoEntity;
import com.paper.epaper.pages.service.PagesRequestMetadataService;
import com.querydsl.core.types.Predicate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


@RestController()
@RequestMapping("/pages/info")
public class PagesRequestController {

    private final PagesRequestMetadataService pagesRequestMetadataService;

    @Autowired
    public PagesRequestController(PagesRequestMetadataService pagesRequestMetadataService) {
        this.pagesRequestMetadataService = pagesRequestMetadataService;
    }

    @PostMapping(value = "/persist")
    public ResponseEntity<Void> processPageRequest(@RequestParam("pagesRequestFile") MultipartFile pagesRequestFile) {
        pagesRequestMetadataService.processPagesRequestFile(pagesRequestFile);
        return ResponseEntity.ok().build();
    }


    @GetMapping("/{id}")
    public ResponseEntity<PagesRequestInfoEntity> getPagesRequestById(@PathVariable Long id) {
        return ResponseEntity.ok(pagesRequestMetadataService.getPagesRequestById(id));
    }

    @GetMapping()
    public Page<PagesRequestInfoEntity> findAllPagesRequestInfos(@PageableDefault Pageable pageable) {
        return pagesRequestMetadataService.findAll(pageable);
    }

    /**
     * Return pagesRequest with pagination and filtering
     * Supports QueryDSL filtering
     * More info <a href="https://gt-tech.bitbucket.io/spring-data-querydsl-value-operators/README.html">...</a>
     *
     * @param predicate QueryDSL predicate
     * @param pageable  page settings
     * @return List of PagesRequestInfo
     */
    @GetMapping("/search")
    public Iterable<PagesRequestInfoEntity> searchPagesRequestInfos(@QuerydslPredicate(root = PagesRequestInfoEntity.class)
                                                                    Predicate predicate, Pageable pageable) {
        return pagesRequestMetadataService.findAllWithPredicate(predicate, pageable);
    }
}
