package com.paper.epaper.pages.controller;

import com.paper.epaper.pages.dto.ErrorDetails;
import com.paper.epaper.pages.dto.ErrorType;
import com.paper.epaper.pages.exceptions.InvalidXmlStructureException;
import com.paper.epaper.pages.exceptions.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@Slf4j
public class RootControllerAdvice {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorDetails> globalExceptionHandling(Exception exception, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder()
                .message(exception.getMessage())
                .errorType(ErrorType.INTERNAL_ERROR)
                .details(request.getDescription(false))
                .build();
        log.error(errorDetails.toString(), exception);
        return new ResponseEntity<>(errorDetails, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(InvalidXmlStructureException.class)
    public ResponseEntity<ErrorDetails> handleInvalidXmlStructure(Exception exception, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder()
                .message(exception.getMessage())
                .errorType(ErrorType.INVALID_INPUT)
                .details(request.getDescription(false))
                .build();
        log.error(errorDetails.toString(), exception);
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ErrorDetails> handleNoResourceStructure(Exception exception, WebRequest request) {
        ErrorDetails errorDetails = ErrorDetails.builder()
                .message(exception.getMessage())
                .errorType(ErrorType.NOT_FOUND)
                .details(request.getDescription(false))
                .build();
        log.error(errorDetails.toString(), exception);
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

}
