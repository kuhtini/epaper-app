package com.paper.epaper.pages.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@ToString
public class ErrorDetails {
    @Builder.Default
    private String uuid = UUID.randomUUID().toString();
    private ErrorType errorType;
    private String message;
    private String details;
}
