package com.paper.epaper.pages.dto;

public enum ErrorType {
    INTERNAL_ERROR, INVALID_INPUT, NOT_FOUND
}
