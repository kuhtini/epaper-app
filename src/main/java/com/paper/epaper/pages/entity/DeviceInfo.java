package com.paper.epaper.pages.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.*;

/**
 * Request device info
 */
@Embeddable
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class DeviceInfo {

    /**
     * Screen width
     */
    @Column(name = "WIDTH")
    private Short width;

    /**
     * Screen height
     */
    @Column(name = "HEIGHT")
    private Short height;

    /**
     * Screen DPI (Dots per inch)
     */
    @Column(name = "DPI")
    private Short dpi;
}
