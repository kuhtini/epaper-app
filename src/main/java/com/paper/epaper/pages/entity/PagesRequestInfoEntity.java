package com.paper.epaper.pages.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

/**
 * Pages request meta information
 */
@Entity
@Table(name = "PAGES_REQUEST_INFO_ENTITY")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PagesRequestInfoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "FILE_NAME")
    private String fileName;

    /**
     * Upload date time to the system
     */
    @Column(name = "UPLOAD_TIME")
    private LocalDateTime uploadTime;

    /**
     * Common request metadata
     */
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "REQUEST_METADATA_ENTITY_ID")
    private RequestMetadataEntity requestMetadataEntity;

}
