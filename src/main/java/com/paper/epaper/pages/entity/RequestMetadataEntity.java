package com.paper.epaper.pages.entity;

import jakarta.persistence.*;
import lombok.*;

/**
 * Request metadata entity
 */
@Entity
@Table(name = "REQUEST_METADATA_ENTITY")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RequestMetadataEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID", nullable = false)
    private Long id;

    /**
     * Application newspaper name
     */
    @Column(name = "NEWSPAPER_NAME")
    private String newspaperName;

    /**
     * Request device info
     */
    @Embedded
    private DeviceInfo deviceInfo;
}
