package com.paper.epaper.pages.exceptions;

public class InvalidXmlStructureException extends RuntimeException {

    public InvalidXmlStructureException(String message, Exception e) {
        super(message, e);
    }
}
