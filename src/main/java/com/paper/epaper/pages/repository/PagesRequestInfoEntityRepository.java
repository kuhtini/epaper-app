package com.paper.epaper.pages.repository;

import com.paper.epaper.pages.entity.PagesRequestInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

public interface PagesRequestInfoEntityRepository extends JpaRepository<PagesRequestInfoEntity, Long>,
        QuerydslPredicateExecutor<PagesRequestInfoEntity> {
}
