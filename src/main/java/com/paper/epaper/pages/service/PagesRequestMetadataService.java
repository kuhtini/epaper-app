package com.paper.epaper.pages.service;

import com.paper.epaper.model.EpaperRequest;
import com.paper.epaper.pages.entity.DeviceInfo;
import com.paper.epaper.pages.entity.PagesRequestInfoEntity;
import com.paper.epaper.pages.entity.RequestMetadataEntity;
import com.paper.epaper.pages.exceptions.InvalidXmlStructureException;
import com.paper.epaper.pages.exceptions.ResourceNotFoundException;
import com.paper.epaper.pages.repository.PagesRequestInfoEntityRepository;
import com.querydsl.core.types.Predicate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;

@Service
@Slf4j
public class PagesRequestMetadataService {

    private final XmlReaderService xmlReaderService;
    private final PagesRequestInfoEntityRepository pagesRequestInfoEntityRepository;

    @Autowired
    PagesRequestMetadataService(XmlReaderService xmlReaderService,
                                PagesRequestInfoEntityRepository pagesRequestInfoEntityRepository) {
        this.xmlReaderService = xmlReaderService;
        this.pagesRequestInfoEntityRepository = pagesRequestInfoEntityRepository;
    }


    /**
     * Process file with xml request
     *
     * @param pagesRequestFile file with xml request structure
     * @throws InvalidXmlStructureException in case if xml structure is not valid
     */
    public void processPagesRequestFile(MultipartFile pagesRequestFile) throws InvalidXmlStructureException {
        log.info("Start processing of page request file '{}'", pagesRequestFile.getOriginalFilename());
        EpaperRequest request = xmlReaderService.convertFromXml(pagesRequestFile, EpaperRequest.class);
        RequestMetadataEntity requestMetadataEntity = buildRequestMetadataEntity(request);
        PagesRequestInfoEntity pagesRequestInfoEntity = PagesRequestInfoEntity.builder()
                .fileName(pagesRequestFile.getOriginalFilename())
                .uploadTime(LocalDateTime.now())
                .requestMetadataEntity(requestMetadataEntity)
                .build();

        pagesRequestInfoEntityRepository.save(pagesRequestInfoEntity);
        log.info("Finished processing of page request file '{}'. Save info", pagesRequestFile.getOriginalFilename());
    }

    private static RequestMetadataEntity buildRequestMetadataEntity(EpaperRequest request) {
        String newspaperName = request.getDeviceInfo().getAppInfo().getNewspaperName();
        DeviceInfo deviceInfo = getDeviceInfo(request.getDeviceInfo().getScreenInfo());

        return RequestMetadataEntity.builder()
                .newspaperName(newspaperName)
                .deviceInfo(deviceInfo)
                .build();
    }

    private static DeviceInfo getDeviceInfo(EpaperRequest.DeviceInfo.ScreenInfo screenInfo) {
        return DeviceInfo.builder()
                .dpi(screenInfo.getDpi())
                .width(screenInfo.getWidth())
                .height(screenInfo.getHeight())
                .build();
    }

    public PagesRequestInfoEntity getPagesRequestById(Long id) {
        return pagesRequestInfoEntityRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("No PagesRequestInfo by id=" + id));
    }

    public Page<PagesRequestInfoEntity> findAll(Pageable pageable) {
        return pagesRequestInfoEntityRepository.findAll(pageable);
    }

    public Iterable<PagesRequestInfoEntity> findAllWithPredicate(Predicate predicate, Pageable pageable) {
        return pagesRequestInfoEntityRepository.findAll(predicate, pageable);
    }
}
