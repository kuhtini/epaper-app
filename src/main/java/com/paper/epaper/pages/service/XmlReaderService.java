package com.paper.epaper.pages.service;

import com.paper.epaper.pages.exceptions.InvalidXmlStructureException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;
import java.io.InputStream;

@Component
@Slf4j
public class XmlReaderService {

    @Value("classpath:request-schema.xsd")
    private Resource requestXsdSchema;

    /**
     * Convert xml content of an input stream to the java object representations
     *
     * @param xmlSource input stream source of xml content
     * @param clazz     target class type that represents xml structure
     * @param <T>       Target class
     * @return clazz instance that represents xml content
     * @throws InvalidXmlStructureException in case when xml is not valid
     */
    public <T> T convertFromXml(InputStreamSource xmlSource, Class<T> clazz) throws InvalidXmlStructureException {
        log.trace("Try to process xml input stream for {}", clazz.getName());
        try (InputStream xmlInputStream = xmlSource.getInputStream()) {
            validateXml(xmlSource);
            Unmarshaller unmarshaller = JAXBContext.newInstance(clazz)
                    .createUnmarshaller();
            Object object = unmarshaller.unmarshal(xmlInputStream);
            return clazz.cast(object);
        } catch (JAXBException e) {
            throw new RuntimeException("Error during XML processing: " + e.getMessage(), e);
        } catch (SAXException e) {
            throw new InvalidXmlStructureException("XML content could not be converted to appropriate model: " + e.getMessage(), e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Check if xml source match the xsd schema
     *
     * @param xmlSource input stream to validate
     * @throws IOException  In case of error during getting schema file
     * @throws SAXException validator exception, if xml is not valid according to the requestXsdSchema
     */
    private void validateXml(InputStreamSource xmlSource) throws IOException, SAXException {
        try (InputStream xmlSourceInputStream = xmlSource.getInputStream()) {
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new StreamSource(requestXsdSchema.getInputStream()));
            schema.newValidator().validate(new StreamSource(xmlSourceInputStream));
        }
    }
}
