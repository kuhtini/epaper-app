package com.paper.epaper.pages.service;

import com.paper.epaper.model.EpaperRequest;
import com.paper.epaper.pages.entity.PagesRequestInfoEntity;
import com.paper.epaper.pages.repository.PagesRequestInfoEntityRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PagesRequestMetadataServiceTest {

    @Mock
    private XmlReaderService xmlReaderService;
    @Mock
    private PagesRequestInfoEntityRepository pagesRequestInfoEntityRepository;

    @InjectMocks
    PagesRequestMetadataService pagesRequestMetadataService;

    @Test
    void processPagesRequestFile_shouldSaveRequestMetaFieldsFromXml() {
        final String fileName = "test.xml";
        final String newspaperName = "test.xml";
        final Short width = 1280;
        final Short height = 720;
        final Short dpi = 333;

        final MultipartFile multipartFile = mock(MultipartFile.class);
        when(multipartFile.getOriginalFilename()).thenReturn(fileName);

        ArgumentCaptor<PagesRequestInfoEntity> entityArgumentCaptor = ArgumentCaptor.forClass(PagesRequestInfoEntity.class);
        when(pagesRequestInfoEntityRepository.save(entityArgumentCaptor.capture())).thenReturn(mock(PagesRequestInfoEntity.class));

        EpaperRequest epaperRequest = new EpaperRequest();
        EpaperRequest.DeviceInfo deviceInfo = new EpaperRequest.DeviceInfo();
        EpaperRequest.DeviceInfo.AppInfo appInfo = new EpaperRequest.DeviceInfo.AppInfo();
        appInfo.setNewspaperName(newspaperName);
        deviceInfo.setAppInfo(appInfo);
        EpaperRequest.DeviceInfo.ScreenInfo screenInfo = new EpaperRequest.DeviceInfo.ScreenInfo();
        screenInfo.setDpi(dpi);
        screenInfo.setHeight(height);
        screenInfo.setWidth(width);
        deviceInfo.setScreenInfo(screenInfo);
        epaperRequest.setDeviceInfo(deviceInfo);

        when(xmlReaderService.convertFromXml(multipartFile, EpaperRequest.class)).thenReturn(epaperRequest);

        pagesRequestMetadataService.processPagesRequestFile(multipartFile);

        PagesRequestInfoEntity actualSavedValue = entityArgumentCaptor.getValue();
        assertEquals(width, actualSavedValue.getRequestMetadataEntity().getDeviceInfo().getWidth(), "Width should match");
        assertEquals(height, actualSavedValue.getRequestMetadataEntity().getDeviceInfo().getHeight(), "Height should match");
        assertEquals(dpi, actualSavedValue.getRequestMetadataEntity().getDeviceInfo().getDpi(), "DPI should match");
        assertEquals(fileName, actualSavedValue.getFileName(), "fileName should match");
        assertEquals(newspaperName, actualSavedValue.getRequestMetadataEntity().getNewspaperName(), "newspaperName should match");
    }

    @Test
    void getPagesRequestById() {
        final long id = 1;
        PagesRequestInfoEntity expectedResult = mock(PagesRequestInfoEntity.class);
        when(pagesRequestInfoEntityRepository.findById(id)).thenReturn(Optional.of(expectedResult));
        PagesRequestInfoEntity actualResult = pagesRequestMetadataService.getPagesRequestById(id);

        assertEquals(expectedResult, actualResult, "Should provide entity from repository");
    }

    @Test
    void findAll() {
        final Pageable pageable = mock(Pageable.class);
        Page<PagesRequestInfoEntity> expectedResult = mock(Page.class);
        when(pagesRequestInfoEntityRepository.findAll(pageable)).thenReturn(expectedResult);
        Page<PagesRequestInfoEntity> actualResult = pagesRequestMetadataService.findAll(pageable);

        assertEquals(expectedResult, actualResult, "Should provide page from repository");
    }
}
