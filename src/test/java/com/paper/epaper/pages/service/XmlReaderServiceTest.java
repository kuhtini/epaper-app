package com.paper.epaper.pages.service;

import com.paper.epaper.model.EpaperRequest;
import com.paper.epaper.pages.exceptions.InvalidXmlStructureException;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Unmarshaller;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.InputStreamSource;
import org.springframework.core.io.Resource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class XmlReaderServiceTest {

    static private MockedStatic<JAXBContext> jaxbContextMockedStatic;
    static private MockedStatic<SchemaFactory> schemaFactoryMockedStatic;
    @Mock
    private Resource requestXsdSchema;
    @InjectMocks
    private XmlReaderService xmlReaderService;

    @BeforeEach
    void beforeEach() {
        jaxbContextMockedStatic = mockStatic(JAXBContext.class);
        schemaFactoryMockedStatic = mockStatic(SchemaFactory.class);
    }

    @Test
    void convertFromXml_ShouldReturnConvertedObject() throws JAXBException, IOException, SAXException {

        EpaperRequest expectedObject = new EpaperRequest();
        InputStreamSource xmlSource = mock(InputStreamSource.class);
        InputStream xmlInputStream = mock(InputStream.class);
        when(xmlSource.getInputStream()).thenReturn(xmlInputStream);

        when(requestXsdSchema.getInputStream()).thenReturn(mock(InputStream.class));

        JAXBContext jaxbContext = mock(JAXBContext.class);
        Unmarshaller unmarshaller = mock(Unmarshaller.class);
        when(jaxbContext.createUnmarshaller()).thenReturn(unmarshaller);
        when(unmarshaller.unmarshal(xmlInputStream)).thenReturn(expectedObject);

        SchemaFactory schemaFactory = mock(SchemaFactory.class);
        Schema schema = mock(Schema.class);
        Validator validator = mock(Validator.class);
        doNothing().when(validator).validate(any(Source.class));
        when(schema.newValidator()).thenReturn(validator);
        when(schemaFactory.newSchema(any(Source.class))).thenReturn(schema);

        jaxbContextMockedStatic.when(() -> JAXBContext.newInstance(any(Class.class))).thenReturn(jaxbContext);
        schemaFactoryMockedStatic.when(() -> SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)).thenReturn(schemaFactory);


        EpaperRequest epaperRequest = xmlReaderService.convertFromXml(xmlSource, EpaperRequest.class);
        assertEquals(expectedObject, epaperRequest, "Should provide object from converter");
    }

    @Test()
    void convertFromXml_ShouldThrowExceptionIfSourceXmlIsNotValid() throws IOException, SAXException {

        InputStreamSource xmlSource = mock(InputStreamSource.class);
        InputStream xmlInputStream = mock(InputStream.class);
        when(xmlSource.getInputStream()).thenReturn(xmlInputStream);

        when(requestXsdSchema.getInputStream()).thenReturn(mock(InputStream.class));

        SchemaFactory schemaFactory = mock(SchemaFactory.class);
        Schema schema = mock(Schema.class);
        Validator validator = mock(Validator.class);
        doThrow(new SAXException("[Test] wrong xml")).when(validator).validate(any(Source.class));
        when(schema.newValidator()).thenReturn(validator);
        when(schemaFactory.newSchema(any(Source.class))).thenReturn(schema);

        schemaFactoryMockedStatic.when(() -> SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)).thenReturn(schemaFactory);

        assertThrows(InvalidXmlStructureException.class, () -> xmlReaderService.convertFromXml(xmlSource, EpaperRequest.class));
    }


    @AfterEach
    void afterEach() {
        jaxbContextMockedStatic.close();
        schemaFactoryMockedStatic.close();
    }
}
